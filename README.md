# MP3 AWS encryption

AWS Lambda function whose main goal is to encrypt files from
source S3 bucket and store them in destination S3 bucket.

Lambda function is a serverless compute service that runs code in response to events.
It runs in isolated instance, and AWS takes care to scale to many instances, if needed.

## Structure

Core functionality of encrypting files is placed in ```encryption.js```, a NodeJS implementation.

```node_modules``` directory should contain all module dependencies used in source, except [AWS-SDK](https://aws.amazon.com/sdk-for-node-js/) 
which is provided by default, in instance running engine.

## Configuration

In order to set up system, it's necessary to zip ```encryption.js``` together with ```node_modules``` folder
and upload it using AWS console as a source of new lambda function.

```encryption.js``` exposes *encrypt.handler* which should be configured as handler for new lambda function.

Recommended basic settings are:

* 2 minutes timeout, since encryption of relatively large mp3 files can take some time

* 1024 MB of memory, for the same reason

## Buckets

Script works with three buckets:

1. Source bucket
2. Key bucket
3. Destination bucket 

**Source bucket** should be the one where raw mp3 files are uploaded. It should be accessible only by lambda function, for maximum security.
Also, source bucket is configured as trigger event for lambda function. Eache time new item is added, encryption lambda is invoked, and
(item,bucket) values are automatically passed as parameters into function.

**Destination bucket** is the target bucket where encrypted mp3 files will be stored. This is where encrypted files will be served from - a public bucket.

**Key bucket** holds shared AES 256bit encryption key in hex encoded format. It should be ultra secure and *only accessible by lambda function*.
Copy of this key is also known to streaming clients.

NOTE: Source bucket name, and source bucket file to process are available from event message. Destination and key bucket's identifiers are hard-coded
into the js file.


