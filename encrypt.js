// dependencies
var async = require('async');
var AWS = require('aws-sdk');
var aesjs = require('aes-js');
var cryptorandom = require('crypto-random-string');
var sha256 = require('js-sha256');
var util = require('util');

// get reference to S3 client
var s3 = new AWS.S3();

// Lambda handler!
exports.handler = function(event, context, callback) {

    // Bucket names, check this! (hard coded or from event data?)
    var srcBucket = event.Records[0].s3.bucket.name;
    var destBucket = "im-drm-encrypted";
    
    // Object key may have spaces or unicode non-ASCII characters ??
    var srcKey = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "));
    
    // Extract file name (dont worry about extension, lambda triggers only for .mp3 files)
    var fileName = srcKey.substring(0, srcKey.lastIndexOf('.'));
    if(!fileName) {
        callback("ERROR: File name empty!");
        return;
    }

    // Download mp3 from source bucket, encrypt and upload to destination bucket.
    async.waterfall(
        [
            function downloadKey(next) {
                s3.getObject(
                    {
                        Bucket: "im-drm-keystore",
                        Key: "key"
                    },
                    function (err, data) {
                        next(err, data.Body.toString("utf-8"));
                    }
                );  
            },
            function donwloadMP3(commonKey, next) {
                console.log("common key: " + commonKey);
                // Download mp3 from S3 into buffer
                s3.getObject(
                    {
                        Bucket: srcBucket, 
                        Key: srcKey
                    }, 
                    function (err, response) {
                        next(err, commonKey, response);
                    }
                );
            },
            function encrypt(commonKey, response, next) {
                var commonKeyBytes = aesjs.utils.hex.toBytes(commonKey);
                
                var localKey  = cryptorandom(64);
                var localKeyBytes = aesjs.utils.hex.toBytes(localKey);
                console.log("local key: " + localKey);
                
                var finalKey  = sha256(commonKey + localKey);
                var finalKeyBytes = aesjs.utils.hex.toBytes(finalKey);
                console.log("final key: " + finalKey);
                
                var ivPrefix = cryptorandom(24)
                var iv = ivPrefix + "00000000";
                var ivBytes = aesjs.utils.hex.toBytes(iv);

                var mp3AES = new aesjs.ModeOfOperation.ctr(finalKeyBytes, ivBytes);
                var localKeyAES = new aesjs.ModeOfOperation.ctr(commonKeyBytes, ivBytes);
                
                var encryptedLocalKey = aesjs.utils.hex.fromBytes(
                    localKeyAES.encrypt(localKeyBytes)
                );

                var destKey = fileName + "_" + encryptedLocalKey + "_" + ivPrefix + ".mp3";
                var encryptedData = mp3AES.encrypt(response.Body);

                next(null, destKey, encryptedData);
            },
            function upload(fileName, data, next) {
                s3.putObject(
                    {
                        Bucket: destBucket,
                        Key: fileName,
                        Body: Buffer.from(data)
                    }, 
                    next
                );
            }
        ],
        function (err) {
            if (err) {
                console.error(
                    'Encrypting ' + srcBucket + '/' + srcKey +
                    ' and uploading to ' + destBucket +
                    ' failed due to an error: ' + err
                );
            } else {
                console.log(
                    'Successfully encrypted ' + srcBucket + '/' + srcKey +
                    ' and uploaded to ' + destBucket
                );
            }

            callback(null, "success");
        }
    );

};